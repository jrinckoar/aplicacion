import { Injectable } from '@angular/core';
import { Producto } from '../model/producto';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductoService {
  public carrito: Array<Producto> = [];
  public serverUrl = 'http://192.168.0.13:3000';
  constructor(private httpClient: HttpClient) {}

  public obtenerTodos() {
    return this.httpClient.get<Producto[]>(this.serverUrl + '/productos');
  }

  public obtenerPorId(id: string) {
    return this.httpClient.get<Producto>(this.serverUrl + id);
  }

  public agregar(prod: Producto) {
    return this.httpClient.post(this.serverUrl + '/productos', prod);
  }

  public getCarrito() {
    return this.carrito;
  }
}
