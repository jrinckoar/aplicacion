import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../model/producto';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  private productos;
  private carrito: Array<Producto>;
  private cantidad = 0;

  constructor(
    private prodSrv: ProductoService,
    private alCtrl: AlertController,
    private loadCtrl: LoadingController
  ) {}

  public async ngOnInit() {
    this.carrito = this.prodSrv.carrito;
    const loading = await this.loadCtrl.create();

    loading.present();
    this.prodSrv.obtenerTodos().subscribe((datos) => {
      this.productos = datos;
      loading.dismiss();
    });
  }

  public async verCarrito() {
    let total = 0;
    let cuerpo = '';
    for (const prod of this.prodSrv.carrito) {
      total += prod.precio;
      cuerpo = cuerpo + prod.nombre + '<br>';
    }
    const cuerpoAlerta = {
      header: 'Título',
      subHeader: 'subtítulo',
      message: cuerpo + '<br>Precio total:' + total,
      buttons: ['ok'],
    };
    const alert = await this.alCtrl.create(cuerpoAlerta);
    await alert.present();
  }
}
