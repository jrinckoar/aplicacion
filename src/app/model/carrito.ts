import {Producto} from './producto';

export class Carrito {

    public id: string;
    public productos: Array<Producto> = [];
    public cantidad: number;
    public total: number;
}
