import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from '../model/producto';
import { ProductoService } from '../services/producto.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {
  private producto = new Producto();
  constructor(
    private activeteRoute: ActivatedRoute,
    private prodSrv: ProductoService
  ) {}

  public ngOnInit() {
    this.activeteRoute.paramMap.subscribe((paramMap) => {
      this.prodSrv.obtenerPorId(paramMap.get('id')).subscribe((datos) => {
        this.producto = datos;
      });
    });
  }

  public agregarCarrito(): void {
    this.prodSrv.carrito.push(this.producto);
  }

  public agregarProducto() {
    this.producto = new Producto();
    this.producto.id = '12';
    this.producto.nombre = 'Hola';
    this.producto.precio = 1;
    this.producto.cantidad = 1;
    this.producto.imagen = 'https';
    // this.prodSrv.agregar(this.producto).subscribe((nuevo) => {
    //   console.log(nuevo);
    // });
  }
}
